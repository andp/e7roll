import React, { useState } from "react";

const roundAmount = value => value.toFixed(2);
const COV_GOLD_AMOUNT = 184000;
const MYSTIC_GOLD_AMOUNT = 280000;

const Home = () => {
  const [rolls, setRolls] = useState(0);
  const [covAmount, setCovAmount] = useState(0);
  const [mysticAmount, setMysticAmount] = useState(0);
  return (
    <div className="container">
      <main>
        <h1>Shop Tracker</h1>
        <div className="card">
          <p>Rolls: {rolls}</p>
          <p>Covenants: {covAmount * 5}</p>
          <p>Mystics: {mysticAmount * 50}</p>
          <button className="roll-button" onClick={() => setRolls(rolls + 1)}>Roll</button>
        </div>
        <div className="card">
          <p>Drops:</p>
          <button className="item-button covenant-button" onClick={() => setCovAmount(covAmount + 1)} />
          <button className="item-button mystic-button" onClick={() => setMysticAmount(mysticAmount + 1)} />
        </div>
        <div className="card">
          <h2>Costs:</h2>
          <p>Total Spent: {rolls * 3}</p>
          {!!covAmount &&
            <div>
              <p>Covenant:</p>
              <p>SS: {roundAmount((rolls * 3) / (covAmount))}</p>
              <p>Gold: {covAmount * COV_GOLD_AMOUNT}</p>
            </div>
          }
          {!!mysticAmount &&
            <div>
              <p>Mystic:</p>
              <p>SS: {roundAmount((rolls * 3) / (mysticAmount))}</p>
              <p>Gold: {mysticAmount * MYSTIC_GOLD_AMOUNT}</p>
            </div>
          }
          {/*<p>Typical cost per roll: {950/10}</p>*/}
        </div>
      </main>
      <style jsx>{`
      .container {
        min-height: 100vh;
        padding: 0 0.5rem;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
      main {
        padding: 5rem 0;
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
      a {
        color: inherit;
        text-decoration: none;
      }
      
      p {
        text-align: left;
      }
      .roll-button {
        padding: 1.5rem;
        font-size: 1.25rem;
        width: 100%;
        box-shadow: none;
        background: #ccc;
        border-radius: 10px;
        margin-top: 1rem;
    }

      code {
        background: #fafafa;
        border-radius: 5px;
        padding: 0.75rem;
        font-size: 1.1rem;
        font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
          DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
      }

      .grid {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;

        max-width: 800px;
        margin-top: 3rem;
      }
      .item-button {
        width: 50px;
        height: 50px;
        border: none;
        background-size: cover;
      }
      .covenant-button {
        background-image: url('https://epic7x.com/wp-content/uploads/2019/01/Covenant-Bookmark.png')
      }
      .mystic-button {
        background-image: url('https://epic7x.com/wp-content/uploads/2019/05/Mystic-Medals.png')
      }
      .card {
        margin: 1rem;
        flex-basis: 100%;
        width: 100%;
        padding: 1.5rem;
        text-align: left;
        color: inherit;
        text-decoration: none;
        border: 1px solid #eaeaea;
        border-radius: 10px;
        transition: color 0.15s ease, border-color 0.15s ease;
      }
      
      .card p {
        margin: 0;
        font-size: 1.25rem;
        line-height: 1.5;
      }

      @media (max-width: 600px) {
        .grid {
          width: 100%;
          flex-direction: column;
        }
      }
    `}</style>

      <style jsx global>{`
      html,
      body {
        padding: 0;
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
          Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
      }

      * {
        box-sizing: border-box;
      }
    `}</style>
    </div>
  );
};

export default Home
